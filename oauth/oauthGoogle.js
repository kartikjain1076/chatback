const passport = require('passport');
const googleStatergy = require('passport-google-oauth20');
const api = require('../api/api');

passport.serializeUser((user,done) => {
    done(null,user);
});

passport.deserializeUser((user,done) => {
    done(null,user);
})

passport.use(
    new googleStatergy({
    callbackURL : '/oauth/googleResponse',
    clientID : '484113242526-vr0gqr6sf12jsivnbjo1o56fqh9ndajl.apps.googleusercontent.com',
    clientSecret : 'yRibDQXNYnN-ddxv6p0BS_5u'
}, (accessToken, refershToken, profile, done) => {
    const userData = {password : null , username : profile.name.givenName, name : profile.displayName , email : profile.emails[0].value};
    console.log(userData);
    const response = api.passportAuth(userData);
    done(null,userData);
}))