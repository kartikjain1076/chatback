const mongoose = require('mongoose');

const register = mongoose.Schema({
	username : String,
	password : String,
	email : String,
	name : String,
	socket_id : {type : String , default : null},
	online : {type : String , default : 'false'},
    verified : {type : Boolean, default : false}
});

module.exports = mongoose.model('register',register);