const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const router = require('./router/router');
const socket = require('socket.io');
const api = require('./api/socketApi');
const fetchRouter = require('./router/fetchRouter');
const oauthRouter = require('./router/oauthRouter');
const googleOauthSetup = require('./oauth/oauthGoogle');
const passport = require('passport');
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyparser.urlencoded({extended : true}));
app.use(bodyparser.json());


mongoose.connect('mongodb://kartikjain:JUSTjoking1#@cluster0-shard-00-00-web08.mongodb.net:27017,cluster0-shard-00-01-web08.mongodb.net:27017,cluster0-shard-00-02-web08.mongodb.net:27017/chat?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin',{ useNewUrlParser: true });
app.use('/fetch',fetchRouter);
app.use('/oauth',oauthRouter);
app.use('/',router);


var port = process.env.PORT || 1337;


const connection = app.listen(port,function(){
    console.log('connected to port',port);
});

const io = socket(connection);

io.on('connection',(socket)=>{
    io.sockets.emit('user_connect','ok');
    console.log('connection made',socket.id);
    
    
    socket.on('disconnect',function(){
        console.log('disconnected : ',socket.id);
        api.disconnect({socket_id : socket.id},function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                io.sockets.emit('userDisconnected',"ok");
            }
        })
    });

    socket.on('disconnectIt',function(){
        console.log('disconnected : ',socket.id);
        api.disconnect({socket_id : socket.id},function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                io.sockets.emit('userDisconnected',"ok");
            }
        })
    });

    socket.on('userConnect',function(data){
        console.log('data',data);
        api.connect({socket_id : socket.id , username : data},function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                io.sockets.emit('userConnected','ok');
            }
        })
    });

    socket.on('sendMessage',function(data){
        console.log('send message',data);
        api.saveMessage(data,function(err,result){
            if(err)
            {
                Console.log(err);
            }
            else
            {
                io.sockets.emit('getMessage','ok');
            }
        })
    });

    socket.on('sendRead',function(data){
        console.log('sendRead',data);
        api.read(data,function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                io.sockets.emit('sentRead','ok');
            }
        })
    });

    socket.on('createGroup',function(data){
        console.log('createGroup',data);
        api.createGroup(data,function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                io.sockets.emit('groupCreated','ok');
            }
        })
    });
})