const register_schema = require('../schema/register_schema');
const message_schema = require('../schema/message_schema');
const GroupSchema = require('../schema/groupSchema');

module.exports = {
    fetch_user : function(data){
        return new Promise((reject,resolve)=>{
            register_schema.find({},function(err,result){
                if(err)
                {
                    console.log(err);
                    reject(err);
                }
                else
                {
                    const userData = {res : 'ok' , body : result};
                    
                            console.log(result);
                            resolve(userData);
                    
                }
            })
        })
    },



fetchMessage : function(data){
    return new Promise(async(reject,resolve)=>{
                await message_schema.updateMany({to : data.from},{delivered : true},async (err,result) => {
                    if(err)
                    {
                        console.log(err);
                        reject(err);
                    }
                    else
                    {
                            console.log(result);
                            await message_schema.find({to : data.to , from : data.from, groupName : data.groupName}).exec(async (err,result) => {
                            if(err)
                            {
                                console.log(err);
                                reject(err);
                            }
                            else
                            {
                                let messageData = {res : 'ok' , body : result};
                                await message_schema.find({to : data.from , from : data.to, groupName : data.groupName}).exec(async (err,result) => {
                                    if(err)
                                    {
                                        console.log(err);
                                        reject(err);
                                    }
                                    else
                                    {
                                        let length = result.length;
                                        for(let i = 0 ; i < length ; i++)
                                       await messageData.body.push(result[i]);
                                       await messageData.body.sort(function(a, b){return b.time.localeCompare(a.time)});
                                       length = messageData.body.length;
                                       for(let i = 1 ; i < length ; i++)
                                       {
                                            if(messageData.body[i-1].time == messageData.body[i].time)
                                                {
                                                    messageData.body.splice(i,1);
                                                    length--;
                                                }
                                            console.log("Time",messageData.body[i-1].time)
                                    }
                                       let messageData2 = {res : 'ok' , body : [], messageEnd : false};
                                       let len = messageData.body.length;
                                       if(len <= data.limit)
                                       {
                                           len = len;
                                           messageData2.messageEnd = false
                                            
                                        }
                                       else
                                       {
                                           messageData2.messageEnd = true;
                                        len = data.limit;
                                        }
                                       for(let i = 0 ; i < len ; i++)
                                       {
                                            messageData2.body.push(messageData.body[i]);
                                       }
                                        console.log('messagedata',messageData2);
                                        resolve(messageData2);
                                    }
                                })
                            }
                        })
            }
        })
})
},


fetchGroup : function(data){
    return new Promise((reject,resolve)=>{
        GroupSchema.find({},function(err,result){
            if(err)
            {
                console.log(err);
                reject(err);
            }
            else
            {
                const groupData = {res : 'ok' , body : result};
                
                        console.log(result);
                        resolve(groupData);
                
            }
        })
    })
},

fetchUnreadMessage : function(data){
    return new Promise((reject,resolve)=>{
        message_schema.find({to : data.from, read : false},function(err,result){
            if(err)
            {
                console.log(err);
                reject(err);
            }
            else
            {
                const messageData = {res : 'ok' , body : result};
                
                        console.log(result);
                        resolve(messageData);
                
            }
        })
    })
}





}