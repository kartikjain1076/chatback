const express = require('express');
const router = express.Router();
const passport = require('passport');
const api = require('../api/api');

router.get('/google',passport.authenticate('google',{ successRedirect: '/',scope: [ 'email', 'profile' ] }));

router.get('/googleResponse',passport.authenticate('google'), (req,res) => {
    res.redirect('http://kartikchat.herokuapp.com/passportAuth/'+req.user.email);
});

router.post('/login', async (req,res) => {
	console.log(req.body);

	try{
		let loginResponse = await api.passportLogin(req.body);
		console.log('router',loginResponse);
		res.send(loginResponse);
	}
	catch(err)
	{
		console.log('router',err);
		res.send(err);
	}
});


module.exports = router;