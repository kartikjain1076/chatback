const express = require('express');
const router = express.Router();
const api = require('../api/api');

router.post('/register', async (req,res) => {
	console.log(req.body);

	try{
		let registerResponse = await api.register(req.body);
		console.log('router',registerResponse);
		res.send(registerResponse);
	}
	catch(err)
	{
		console.log('router',err);
		res.send(err);
	}
});

router.post('/login', async (req,res) => {
	console.log(req.body);

	try{
		let loginResponse = await api.login(req.body);
		console.log('router',loginResponse);
		res.send(loginResponse);
	}
	catch(err)
	{
		console.log('router',err);
		res.send(err);
	}
});

router.post('/verify', async(req,res) => {
	try{
		let isVerify = await api.verify(req.body);
		console.log('router try',isVerify);
		res.send(isVerify);
	}
	catch(err)
	{
		console.log('router',err);
		res.send(err);
	}
})



module.exports = router;