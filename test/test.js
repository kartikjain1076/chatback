process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

describe('/POST message', () => {
    it('it should get all the messages', (done) => {
        chai.request('localhost:1337')
        .post('/fetch/fetchMessage')
        .send({to : ''})
        .end((err,res) => {
            res.body.should.have.be.a('object');
            res.body.should.have.property('body');
            res.body.should.have.property('res').eql('ok');
            done();
        });
    });
});

describe('/POST User', () => {
    it('it should get all the Users', (done) => {
        chai.request('localhost:1337')
        .post('/fetch/fetch_user')
        .send({})
        .end((err,res) => {
            res.body.should.have.be.a('object');
            res.body.should.have.property('body');
            res.body.should.have.property('res').eql('ok');
            done();
        });
    });
});

describe('/POST Group', () => {
    it('it should get all the Groups', (done) => {
        chai.request('localhost:1337')
        .post('/fetch/fetchGroup')
        .send({})
        .end((err,res) => {
            res.body.should.have.be.a('object');
            res.body.should.have.property('body');
            res.body.should.have.property('res').eql('ok');
            done();
        });
    });
});